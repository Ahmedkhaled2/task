<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'ahmed khaled',
            'username' => 'admin',
            'phone' => '01111216938',
            'short_description' => 'backend developer',
            'education_degree' => 'computer science',
            'email' => 'admin@task.com',
            'password' => bcrypt('admin'),
            'age' => 25,
        ]);
        $admin->roles()->attach(1);

        $test = User::create([
            'name' => 'test',
            'username' => 'test',
            'phone' => '01111216938',
            'short_description' => 'test',
            'education_degree' => 'test',
            'email' => 'test@task.com',
            'password' => bcrypt('test'),
            'age' => 25,
        ]);

        $permissions = Permission::where('name', 'not like', '%%delete%%')->get();

        Role::create(['name' => 'manger', 'label' => 'manger']);

        foreach ($permissions as $permission) {
            DB::table('role_permissions')->insert([
                'permission_id' => $permission->id,
                'role_id' => '2',
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]);
        }

        $test->roles()->attach(2);


    }
}
