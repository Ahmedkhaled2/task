<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\BlogsController;
use App\Http\Controllers\Api\ProjectsController;
use App\Http\Controllers\Api\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Login Route
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login'])->name('auth.login');
    Route::post('/logout', [AuthController::class, 'logout'])->name('auth.logout');
});


// Users Route
Route::group(['prefix' => 'users'], function () {
    Route::get('/', [UsersController::class, 'index'])->name('users.index');
    Route::post('/store', [UsersController::class, 'store'])->name('users.store');
    Route::group(['prefix' => '{user}'], function () {
        Route::post('/update', [UsersController::class, 'update'])->name('users.update');
        Route::post('/delete', [UsersController::class, 'destroy'])->name('users.destroy');
    });
});

// Blogs Route
Route::group(['prefix' => 'blogs'], function () {
    Route::get('/', [BlogsController::class, 'index'])->name('blogs.index');
    Route::post('/store', [BlogsController::class, 'store'])->name('blogs.store');
    Route::group(['prefix' => '{blog}'], function () {
        Route::post('/update', [BlogsController::class, 'update'])->name('blogs.update');
        Route::post('/delete', [BlogsController::class, 'destroy'])->name('blogs.destroy');
    });
});

// Projects Route
Route::group(['prefix' => 'projects'], function () {
    Route::get('/', [ProjectsController::class, 'index'])->name('projects.index');
    Route::post('/store', [ProjectsController::class, 'store'])->name('projects.store');
    Route::group(['prefix' => '{project}'], function () {
        Route::post('/update', [ProjectsController::class, 'update'])->name('projects.update');
        Route::post('/delete', [ProjectsController::class, 'destroy'])->name('projects.destroy');
    });
});
