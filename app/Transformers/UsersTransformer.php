<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class UsersTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($user)
    {
        return [
            'id' => $user->id??'-',
            'name' => $user->name??'-',
            'username' => $user->username??'-',
            'phone' => $user->phone??'-',
            'email' => $user->email??'-',
            'short_description' => $user->short_description??'-',
            'education_degree' => $user->education_degree??'-',
            'age' => $user->age??'-',
            'role_id' => $user->roles->first()->id??'-',
            'role_title' => $user->roles->first()->name??'-',
        ];
    }
}
