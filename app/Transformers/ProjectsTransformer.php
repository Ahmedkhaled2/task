<?php

namespace App\Transformers;

use Illuminate\Support\Facades\Storage;
use League\Fractal\TransformerAbstract;

class ProjectsTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($project)
    {
        return [
            'id' => $project->id??'-',
            'title' => $project->title??'-',
            'description' => $project->description??'-',
            'image' => Storage::url($project->image)??'-',
            'start_date' => $project->start_date??'-',
            'end_date' => $project->end_date??'-',
        ];
    }
}
