<?php

namespace App\Transformers;

use Illuminate\Support\Facades\Storage;
use League\Fractal\TransformerAbstract;

class BlogsTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform($blog)
    {

        return [
            'id' => $blog->id??'-',
            'title' => $blog->title??'-',
            'description' => $blog->description??'-',
            'image' => Storage::url($blog->image),
        ];
    }
}
