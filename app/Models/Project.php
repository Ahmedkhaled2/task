<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'image', 'start_date', 'end_date'];

    public function setImageAttribute($value): void
    {
        if ($value != null) {
            if (!is_string($value)) {
                $this->image ? Storage::disk('public')->delete($this->image) : null;
                $value = Storage::disk('public')->putFile('projects', $value);
            }
        }
        $this->attributes['image'] = $value;
    }
}
