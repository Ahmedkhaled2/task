<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Blog extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'image'];

    public function setImageAttribute($value): void
    {
        if ($value != null) {
            if (!is_string($value)) {
                $this->image ? Storage::disk('public')->delete($this->image) : null;
                $value = Storage::disk('public')->putFile('blogs', $value);
            }
        }
        $this->attributes['image'] = $value;
    }
}
