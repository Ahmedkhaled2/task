<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBlogsRequest;
use App\Http\Requests\UpdateBlogsRequest;
use App\Models\Blog;
use App\Transformers\BlogsTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 *
 */
class BlogsController extends Controller
{
    /**
     *
     */
    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {

        $this->authorize('read_blog');

        $blogs = Blog::orderBy('created_at', 'desc')->get();

        $blogs = fractal($blogs, new BlogsTransformer());

        return response()->json(['code' => 200, 'message' => 'Data fetched successfully', 'item' => $blogs], 200);
    }


    /**
     * @param CreateBlogsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateBlogsRequest $request)
    {
        $blog = Blog::create($request->all());

        $blog = fractal($blog, new BlogsTransformer());

        return response()->json(['code' => 200, 'message' => 'Data added successfully', 'item' => $blog], 200);

    }


    /**
     * @param UpdateBlogsRequest $request
     * @param Blog $blog
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateBlogsRequest $request, Blog $blog)
    {
        $blog->update($request->all());

        $blog = fractal($blog, new BlogsTransformer());

        return response()->json(['code' => 200, 'message' => 'Data updated successfully', 'item' => $blog], 200);

    }

    /**
     * @param Blog $blog
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Blog $blog)
    {
        $this->authorize('delete_blog');

        $blog->delete();

        return response()->json(['code' => 200, 'message' => 'Data deleted successfully', 'item' => $blog], 200);
    }
}
