<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUsersRequest;
use App\Http\Requests\UpdateUsersRequest;
use App\Models\User;
use App\Transformers\UsersTransformer;
use Illuminate\Http\Request;

/**
 *
 */
class UsersController extends Controller
{
    /**
     *
     */
    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('read_user');

        $users = User::orderBy('created_at', 'desc')->get();

        $users = fractal($users, new UsersTransformer());

        return response()->json(['code' => 200, 'message' => 'Data fetched successfully', 'item' => $users], 200);
    }


    /**
     * @param CreateUsersRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateUsersRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'phone' => $request->phone,
            'short_description' => $request->short_description,
            'education_degree' => $request->education_degree,
            'age' => $request->age,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $user->roles()->attach($request->role_id);

        $user = fractal($user, new UsersTransformer());

        return response()->json(['code' => 200, 'message' => 'Data added successfully', 'item' => $user], 200);

    }


    /**
     * @param UpdateUsersRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateUsersRequest $request, User $user)
    {
        $user->update([
            'name' => $request->name,
            'username' => $request->username,
            'phone' => $request->phone,
            'short_description' => $request->short_description,
            'education_degree' => $request->education_degree,
            'age' => $request->age,
            'email' => $request->email,
        ]);

        if ($request->password) {
            $user->update([
                'password' => bcrypt($request->password)
            ]);
        }
        $user->roles()->sync($request->role_id);

        $user = fractal($user, new UsersTransformer());

        return response()->json(['code' => 200, 'message' => 'Data updated successfully', 'item' => $user], 200);

    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(User $user)
    {
        $this->authorize('delete_user');

        $user->delete();

        return response()->json(['code' => 200, 'message' => 'Data deleted successfully', 'item' => $user], 200);
    }
}
