<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateProjectsRequest;
use App\Http\Requests\UpdateProjectsRequest;
use App\Models\Project;
use App\Transformers\ProjectsTransformer;
use Illuminate\Http\Request;

/**
 *
 */
class ProjectsController extends Controller
{
    /**
     *
     */
    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('read_project');

        $projects = Project::orderBy('created_at', 'desc')->get();

        $projects = fractal($projects, new ProjectsTransformer());

        return response()->json(['code' => 200, 'message' => 'Data fetched successfully', 'item' => $projects], 200);
    }


    /**
     * @param CreateProjectsRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateProjectsRequest $request)
    {
        $project = Project::create($request->all());

        $project = fractal($project, new ProjectsTransformer());

        return response()->json(['code' => 200, 'message' => 'Data added successfully', 'item' => $project], 200);

    }


    /**
     * @param UpdateProjectsRequest $request
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateProjectsRequest $request, Project $project)
    {
        $project->update($request->all());

        $project = fractal($project, new ProjectsTransformer());

        return response()->json(['code' => 200, 'message' => 'Data updated successfully', 'item' => $project], 200);

    }

    /**
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Project $project)
    {
        $this->authorize('delete_project');

        $project->delete();

        return response()->json(['code' => 200, 'message' => 'Data deleted successfully', 'item' => $project], 200);
    }
}
