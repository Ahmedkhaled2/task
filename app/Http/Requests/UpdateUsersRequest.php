<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('update_user', User::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'username' => 'required',
            'phone' => 'required',
            'short_description' => 'required',
            'education_degree' => 'required',
            'age' => 'required',
            'role_id' => 'required',
            'email' => 'required|email|' . ($this->user ? "unique:users,email,{$this->user->id}" : "unique:users,email"),
        ];
    }
}
