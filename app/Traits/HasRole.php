<?php

namespace App\Traits;


use App\Models\Role;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Trait HasRole
 * @package App\Traits
 */
trait HasRole
{
    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id')
            ->withTimestamps();
    }

    /**
     * @param string $role
     * @return void
     */
    public function actAs(string $role): void
    {
        $role = Role::whereName($role)->firstOrFail();

        if (!$this->roles->contains('id', $role->id)) {
            $this->roles()->sync($role);
        }
    }

    /**
     * @param $role
     * @return bool
     */
    public function hasRole($role): bool
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }
        return !!$role->intersect($this->roles)->count();
    }

}
